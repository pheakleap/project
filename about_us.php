<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <title>Exercise2</title>
    <link rel="stylesheet" href="about_us.css">

</head>
<body>
    
    <?php
        $admin = [
            ["Name" => "Sok Sothika", "Job" => "STUDENT", "Role" => "ADMIN", "Image" => "image/me.jpg"],
            ["Name" => "Narith SopheakLeap", "Job" => "STUDENT", "Role" => "ADMIN", "Image" => "image/me.jpg"],
            ["Name" => "Hing Sothida", "Job" => "STUDENT", "Role" => "ADMIN", "Image" => "image/me.jpg"],
            ["Name" => "Lysan Gechleang", "Job" => "STUDENT", "Role" => "ADMIN", "Image" => "image/me.jpg"]
        ];
    ?>
    <div class="container text-center">
    <div class="row g-2">
        <div class="col-6">
            <div class="p-3">
                <img src="<?php echo $admin[0]["Image"]?>" alt="Picture">
                <pre><strong><?php echo $admin[0]["Name"]?></strong></pre>
                <p class="job"><?php echo $admin[0]["Job"]?></p>
                <p class="role"><?php echo $admin[0]["Role"]?></p>
                <hr>
                <a href="thika.html" class="btn btn-dark">View More</a>
            </div>
        </div>
        <div class="col-6">
        <div class="p-3">
            <img src="<?php echo $admin[0]["Image"]?>" alt="Picture">
            <pre><strong><?php echo $admin[0]["Name"]?></strong></pre>
            <p class="job"><?php echo $admin[0]["Job"]?></p>
            <p class="role"><?php echo $admin[0]["Role"]?></p>
            <hr>
            <a href="thika.html" class="btn btn-dark">View More</a>
        </div>
        </div>
        <div class="col-6">
        <div class="p-3">Custom column padding</div>
        </div>
        <div class="col-6">
        <div class="p-3">Custom column padding</div>
        </div>
    </div>
    </div>
    <div class="container px-4 text-center">
    <div class="row gx-5">
        <div class="col">
            <div class="p-3">
            <img src="<?php echo $admin[0]["Image"]?>" alt="Picture">
            <pre><strong><?php echo $admin[0]["Name"]?></strong></pre>
            <p class="job"><?php echo $admin[0]["Job"]?></p>
            <p class="role"><?php echo $admin[0]["Role"]?></p>
            <hr>

            <a href="me.html" class="btn btn-dark">View More</a>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
            <img src="<?php echo $admin[1]["Image"]?>" alt="Picture">
            <pre><strong><?php echo $admin[1]["Name"]?></strong></pre>
            <p class="job"><?php echo $admin[1]["Job"]?></p>
            <p class="role"><?php echo $admin[1]["Role"]?></p>
            <hr>
            <a href="pheakleap.html" class="btn btn-dark">View More</a>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
            <img src="<?php echo $admin[2]["Image"]?>" alt="Picture">
            <pre><strong><?php echo $admin[2]["Name"]?></strong></pre>
            <p class="job"><?php echo $admin[2]["Job"]?></p>
            <p class="role"><?php echo $admin[2]["Role"]?></p>
            <hr>

            <a href="thida.html" class="btn btn-dark">View More</a>
            </div>
        </div>
        <div class="col">
            <div class="p-3">
            <img src="<?php echo $admin[3]["Image"]?>" alt="Picture">
            <pre><strong><?php echo $admin[3]["Name"]?></strong></pre>
            <p class="job"><?php echo $admin[3]["Job"]?></p>
            <p class="role"><?php echo $admin[3]["Role"]?></p>
            <hr>

            <a href="gechleang.html" class="btn btn-dark">View More</a>
            </div>
        </div>
        
    </div>
    

    </div>

</body>
</html>
