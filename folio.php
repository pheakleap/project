<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JOIN US</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
<?php
    require('connection.php');
    include('header.php');
?>
<form action="member.php" method="post" enctype="multipart/form-data">
    <legend style="text-align: center;"><b>Register for Member!</b></legend>
    <h5 style="text-align: center; color: gray; font-size:smaller">We are happy to welcome you on board! Share your wonderful experience with us!</h5>
    <div class="container">
    <div class="form-group">
        <label for="firstname">First Name</label>
        <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Enter firstname"required>
    </div>
    <div class="form-group">
        <label for="lastname">Last Name</label>
        <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Enter lastname"required>
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email" required>
    </div>
    <div>
        <label class="my-1 mr-2" for="inlineFormCustomSelectPref" name = "gender">Gender</label>
        <select class="custom-select my-1 mr-sm-2" name="gender"id="inlineFormCustomSelectPref" required>
            <option selected>Choose...</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
            <option value="Prefer not to say">Prefer not to say</option>
        </select>
    </div>
    
   
    <div class="form-group">
        <label for="">Upload your CV/Resume/Portfolio</label>
        <input id = "pdf" type="file" name = "pdf" vallue = ""required>
    </div>
    <button type="submit" class="btn btn-success" name = "btnSubmit">Submit</button>
    </div>
</form>
<?php

    if(isset($_POST['btnSubmit'])){
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $gender = $_POST['gender'];
        //uploadedFiles
        $pdf=$_FILES['pdf']['name'];
        $pdf_type = $_FILES['pdf']['type'];
        $pdf_size = $_FILES['pdf']['size'];
        $pdf_tem_loc=$_FILES['pdf']['tmp_name'];
        $pdf_store = "uploadedFiles/".$pdf;
        move_uploaded_file($pdf_tem_loc,$pdf_store);
        $sql="INSERT INTO member (firstname,lastname,email,gender,cv_uploaded) VALUES(?,?,?,?,?)";
        $stmt = $con ->prepare($sql);
        $stmt->bind_param("sssss", $firstname,$lastname,$email,$gender,$pdf_store);
        if ($stmt->execute()) {
            header('location: member.php');
            echo "<script>alert('Data Inserted Successfully')</script>";
            exit; // It's good practice to exit after redirection
        } else {
            echo "<script>alert('Failed, try again!')</script>";
        }
        
        
        
    }

    echo "<br>";
    include ('footer.php');
?>
</body>
</html>