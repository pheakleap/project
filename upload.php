<?php
    if(isset($_POST['btnSubmit'])){
        $file = $_FILES['file']; //name of the input of file in folio.php
        //extract info of the files(name,filletype,size)

        $fileName = $file['name'];
        $fileTmpName = $file['tmp_name'];
        $fileSize = $file['size'];
        $fileError = $file['error'];
        $fileType = $file['type'];
        //which files are we gonna allow to upload
        //allow only pdf
        /*by using explode when they name 'name.pdf'it'll explode
        //to name pseng and pdf pseng*/
        $fileExt = explode('.',$fileName);
        /*end() : grab the last value from the explosion which in
        our case is the extension and not the file name*/
        $fileActualExt = strtolower(end($fileExt));

        //tell which file we allow user to upload(warning)
        $allowed = array('pdf','jpg','png','jpeg');
        //check fileActualExt to have the allowed extension
        if (in_array($fileActualExt, $allowed)){
            //if the extension ahve the array run the code:
            if ($fileError === 0){
                if($fileSize < 500000){
                    //we need a unique id inorder for some
                    //ppl that might override an existent file
                    $fileNameNew = uniqid('',true).".".$fileActualExt;
                    $fileDestination = 'uploads/'.$fileNameNew;
                    //move_uploaded_file(filetmpname, where file want togo)
                    move_uploaded_file($fileTmpName,$fileDestination);
                    header("location: folio.php?uploadsuccess");
                }else {
                    echo "Your file is too big!";
                }
            }else {
                echo "There was an error uploading your file!";
            }
        } else {
            echo "You can't upload file with this type!";
        }
    }
?>