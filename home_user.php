<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
    <link rel="stylesheet" href="home_user.css">
</head>
<body>
    <?php
        include('header.php');
    ?>
    <section class="home" id="home">
        <div class="content">
            <h3>Welcome</h3>
            <span> Network and Build Portfolio </span>
            <p> Net&Fo, a community to socialize through networking and sharing 
                Portfolio. Getting to know each other and be supportive as a community.</p>
            <a href="join.php" class="btn"> Join us </a>   
        </div>
        <div class="image">
                <img src="image/net&fo.png" alt="image">
            </div>
    </section>

    <section class="about" id="about">
        <div class="row">
            <div class="cv-container">
                <img src="image/photo1.jpg" alt="cv-template-homepage">
            </div>
            <div class="content">
                <h3>why choose us?</h3>
                <p>Net&Fo is more than just a platform; it's a digital haven where connections flourish and creativity thrives. Within our vibrant community, members bond over shared passions, exchanging ideas and insights that propel each other forward. 
                As individuals share their portfolios, they open windows into their unique talents and experiences, inviting admiration and encouragement from fellow members. It's a place where collaboration sparks innovation and where every success is celebrated, fostering a culture of support and camaraderie. 
                In this dynamic environment, friendships are forged, mentorship flourishes, and dreams take flight. Welcome to Net&Fo, where connections are made, and aspirations are realized, one shared portfolio at a time.</p>
            </div>
            <a href="#" class="btn"> Learn more </a>
        </div>
    </section>

    <section class="about" id="about">
        <h1 class="protocols"> <span> <b>Our Protocols</b></span> </h1>
        <div class="row">
            <div class="content2">
                <table>
                    <ul>
                        <li>Diversity and Respect: We value diversity and treat every member with dignity and respect. Discriminatory language or behavior is not tolerated.</li>
                        <li>Supportive Culture: We encourage a culture of support and encouragement. Whether it's providing feedback on portfolios or offering a helping hand, we uplift one another to achieve our goals.</li>
                        <li>Integrity: Honesty and transparency are paramount. We expect all members to represent themselves truthfully and to respect intellectual property rights.</li>
                        <li>Constructive Communication: We promote open and constructive communication. Disagreements are inevitable, but we resolve them respectfully and focus on finding solutions.</li>
                        <li>Privacy: We prioritize the privacy of our members. Personal information shared within the community remains confidential, and members are expected to respect each other's privacy.</li>
                        <li>Professionalism: We maintain a professional demeanor in all interactions. This includes refraining from spamming, soliciting, or engaging in any form of harassment.</li>
                        <li>Compliance: Members are expected to adhere to the terms of service and community guidelines. Violations may result in disciplinary action, including suspension or expulsion from the community.</li>
                        <li>Continuous Improvement: We are committed to continually improving Net&Fo. Members are encouraged to provide feedback and suggestions for enhancements to better serve the community.</li>
                    </ul>
                </table>
                
            </div>
        </div>
    </section>

</body>
</html>
<?php 
include ('footer.php');
?>