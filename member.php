<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Member</title>
    <link rel = "stylesheet" href = "member.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style>
        #search-title {
            display: inline-block;
            margin-top: 2rem;
            font-size: 1.25rem;
            font-weight: bold;
        }
        .search-icon {
            display: inline-block;
            vertical-align: middle;
            width: 20px; 
            height: 20px;
        }
        .search-bar {
            width: 100%;
            box-sizing: border-box; 
            padding: 10px; 
            border: 1px solid #ccc; 
            border-radius: 5px; 
        }
    </style>
    </style>
</head>
<body>
    <?php
        include("connection.php");
        include("header.php");

    ?>
    <h6 class = "mt-5" id = "search-title"><b>Search</b></h6>
    <img class="search-icon" src="https://cdn3.iconfinder.com/data/icons/feather-5/24/search-512.png" alt="Search Icon"></h6>
    <div class="input-group mb-4 mt-3">
        <div class="form-outline">
            <input type="text" class="search-bar" id="getName"placeholder="Enter your search...">
        </div>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Email</th>
                <th scope="col">Gender</th>
                <th scope="col">Portfolio / CV</th>
                <th scope="col">View</th>
            </tr>
        </thead>
        <tbody id = "showdata">
        <?php
            $sql = "SELECT * FROM member";
            $data = $con->query($sql);
            if($data->num_rows>0){
                while($rows = $data -> fetch_assoc()){
                    $id = $rows['id'];
                    $firstname = $rows['firstname'];
                    $lastname = $rows['lastname'];
                    $gender = $rows['gender'];
                    $email = $rows['email'];
                    $cv_uploaded = $rows['cv_uploaded'];
                    $view = $rows['view'];
                    echo "<tr>
                        <td>$id</td>
                        <td>$firstname</td>
                        <td>$lastname</td>
                        <td>$email</td>
                        <td>$gender</td>
                        <td>$cv_uploaded</td>
                        <td><a href='$cv_uploaded'>View</a></td>
                        </tr>";
                    // echo"<div class='container px-4 text-center'>
                    //     <div class='row gx-5'>
                    //       <div class='col'>
                    //        <div class='p-3'>
                    //             $firstname <br>
                    //             $lastname
                    //             $email
                    //             $gender
                    //             $cv_uploaded
                    //             <a href='$cv_uploaded'>View</a>
                    //         </div>
                    //       </div>
                    //       </div>
                    //     </div>
                    //   </div>";
                }
            }
        ?> 
        </tbody>
    </table>
    
    <script>
        $(document).ready(function(){
            $("#getName").on("keyup", function(){
                var getName = $(this).val();
                
                $.ajax({
                    url: "search.php",
                    method: "POST",
                    data:{firstname: getName},
                    success:function(data)
                    {
                        $("#showdata").html(data);
                    }
                })
            })
        })
    </script>
</body>
</html>