<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" 
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" 
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" 
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="signup.css">
</head>
<body >
    <div class="container">
        <form action="">
        
            <h2><center>LOGIN</center></h2>
            <br>
            <div class="row g-3 align-items-center">
                <div class="col-auto">
                    <label class="col-form-label">Username</label>
                </div>
                <div class="col-auto">
                    <input type="text" class="form-control" placeholder="Username" name="username" required/>
                </div>
            </div>
    
            <div class="row g-3 align-items-center">
                <div class="col-auto">
                    <label class="col-form-label">Password</label>
                </div>
                <div class="col-auto">
                    <input type="password" class="form-control" placeholder="Password" name="password" required/>
                </div>
            </div>
            <br>
            <div class="button">
                <center><button type="submit" class="btn btn-primary me-md-2">LOGIN</button></center>
            </div>
            <br>
            <span class="link"><center><a href="login.php">Do not have an account? Sing Up</a></center></span>
        </form>
    </div>
</body>
</html>